package StepDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

public class StepDefinition {

    WebDriver driver;
    List<Double> initialPrices = new ArrayList<>();

    @Before
    public void globalSetup() {
        System.setProperty("webdriver.gecko.driver", "driver/geckodriver.exe");
        driver = new FirefoxDriver();
    }
    
    @After
    public void teardown(Scenario scenario) {
        if (scenario.isFailed()) {
        }
        if (driver != null) {
            driver.quit();
        }
    }
    
    @Given("the user on {string}")
    public void the_user_enters(String string){
        driver.get(string);
    }
    
    @When("the user login with {string} and {string}")
    public void valid_user(String username, String password){
        driver.findElement(By.cssSelector("*[data-test=\"username\"]")).sendKeys(username);
        driver.findElement(By.cssSelector("*[data-test=\"password\"]")).sendKeys(password);
        driver.findElement(By.cssSelector("*[data-test=\"login-button\"]")).click();
    }

    @Then("the user should be logged in")
    public void user_on_dashboard(){
        assert(driver.findElements(By.cssSelector(".app_logo")).size() > 0);
        assert(driver.findElements(By.cssSelector(".title")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"product_sort_container\"]")).size() > 0);
        assert(driver.findElements(By.cssSelector(".shopping_cart_link")).size() > 0);
        assert(driver.findElements(By.cssSelector(".header_label")).size() > 0);
    }

    @Then("the user should see an error message")
    public void error_msg_login(){
        assert(driver.findElements(By.cssSelector("*[data-test=\"error\"]")).size() > 0);
    }

    @Given("the user on the dashboard")
    public void the_user_on_the_dashboard() {
        driver.get("https://www.saucedemo.com/inventory.html");
    }

    @When("the user clicks logout")
    public void the_user_clicks_logout() {
        driver.findElement(By.id("react-burger-menu-btn")).click();
        driver.findElement(By.id("logout_sidebar_link")).click();
    }
    @Then("the user should be logged out")
    public void the_user_should_be_logged_out() {
        assert(driver.findElements(By.cssSelector(".login_logo")).size() > 0);
        assert(driver.findElements(By.cssSelector(".login_wrapper-inner")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"login-button\"]")).size() > 0);
    }

    @Then("the user should be redirected to product detail page")
    public void the_user_should_be_redirected_to_product_detail_page() {
      assert(driver.findElements(By.cssSelector(".inventory_details_img")).size() > 0);
      assert(driver.findElements(By.cssSelector(".inventory_details_price")).size() > 0);
      assert(driver.findElements(By.cssSelector(".inventory_details_desc")).size() > 0);
      assert(driver.findElements(By.cssSelector(".inventory_details_name")).size() > 0);
      assert(driver.findElements(By.cssSelector("*[data-test=\"back-to-products\"]")).size() > 0);
      assert(driver.findElements(By.cssSelector("*[data-test=\"add-to-cart-sauce-labs-backpack\"]")).size() > 0);
    }

    @When("the user click the product title")
    public void the_user_click_the_product_title() {
        driver.findElement(By.cssSelector("#item_4_title_link > .inventory_item_name")).click();
    }

    @When("the user click on {string} sort button")
    public void the_user_sort(String option) {
        WebElement dropdownElement = driver.findElement(By.xpath("//*[@id='header_container']/div[2]/div/span/select)"));
        dropdownElement.click();
        Select dropdown = new Select(dropdownElement);
        dropdown.selectByValue(option);

        String selectedOption = dropdown.getFirstSelectedOption().getText();
        Assert.assertEquals("Expected Option Text", selectedOption);
    }

    @Then("dashboard page should be sorted")
    public void product_sorted(){
        List<WebElement> productPriceElements = driver.findElements(By.className("inventory_item_price"));
        for (WebElement priceElement : productPriceElements) {
            String priceString = priceElement.getText().replace("$", "");
            initialPrices.add(Double.parseDouble(priceString));
        }

        // Check if prices are sorted in ascending order
        for (int i = 0; i < initialPrices.size() - 1; i++) {
            Assert.assertTrue("Prices are not sorted in ascending order", initialPrices.get(i) <= initialPrices.get(i + 1));
        }
    }

    @Then("the user could see product in cart list")
    public void the_user_could_see_product_in_cart_list() {
        assert(driver.findElements(By.cssSelector(".inventory_item_name")).size() > 0);
        assert(driver.findElements(By.cssSelector(".inventory_item_desc")).size() > 0);
        assert(driver.findElements(By.cssSelector(".inventory_item_price")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"remove-sauce-labs-backpack\"]")).size() > 0);
    }

    @When("clicks the add to cart button")
    public void clicks_the_add_to_cart_button() {
        String actualString = driver.findElement(By.cssSelector("*[data-test=\"add-to-cart-sauce-labs-backpack\"]")).getText();
        if(actualString.contains("Add")){
            driver.findElement(By.cssSelector("*[data-test=\"add-to-cart-sauce-labs-backpack\"]")).click();
        } else {
            System.out.println("Item added!");
        }
    }

    @When("clicks the cart button")
    public void clicks_the_cart_button() {
            driver.findElement(By.cssSelector(".shopping_cart_link")).click();
    }

    @Given("the user shopping cart contains the item")
    public void the_user_shopping_cart_contains_the_item() {
        driver.get("https://www.saucedemo.com/cart.html");
        assert(driver.findElements(By.cssSelector(".inventory_item_name")).size() > 0);
        assert(driver.findElements(By.cssSelector(".inventory_item_desc")).size() > 0);
        assert(driver.findElements(By.cssSelector(".inventory_item_price")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"remove-sauce-labs-backpack\"]")).size() > 0);
    }

    @When("the user click the checkout button")
    public void the_user_click_the_checkout_button() {
        driver.findElement(By.cssSelector("*[data-test=\"checkout\"]")).click();
    }

    @Then("the user should be taken to the checkout page")
    public void the_user_should_be_taken_to_the_checkout_page() {
        assert(driver.findElements(By.cssSelector("*[data-test=\"firstName\"]")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"lastName\"]")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"postalCode\"]")).size() > 0);
        assert(driver.findElements(By.cssSelector("*[data-test=\"continue\"]")).size() > 0);
    }

    @When("the user type their {string}, {string}, and {string}")
    public void the_user_type_their_and(String s, String s2, String s3) {
        driver.findElement(By.cssSelector("*[data-test=\"firstName\"]")).sendKeys("first");
        driver.findElement(By.cssSelector("*[data-test=\"lastName\"]")).sendKeys("last");
        driver.findElement(By.cssSelector("*[data-test=\"postalCode\"]")).sendKeys("zip");
    }

    @When("the user click continue")
    public void the_user_click_continue() {
        driver.findElement(By.cssSelector("*[data-test=\"continue\"]")).click();
    }

    @Then("the user should be taken to order detail page")
    public void the_user_should_be_taken_to_order_detail_page() {
      assert(driver.findElements(By.cssSelector(".summary_info_label:nth-child(1)")).size() > 0);
      assert(driver.findElements(By.cssSelector(".summary_info_label:nth-child(3)")).size() > 0);
      assert(driver.findElements(By.cssSelector(".summary_info_label:nth-child(5)")).size() > 0);
      assert(driver.findElements(By.cssSelector(".summary_total_label")).size() > 0);
    }

    @When("the user click finish")
    public void the_user_click_finish() {
        driver.findElement(By.cssSelector("*[data-test=\"finish\"]")).click();
    }

    @Then("the user should see succesful order message")
    public void the_user_should_see_succesful_order_message() {
        assert(driver.findElements(By.cssSelector(".pony_express")).size() > 0);
        assert(driver.findElements(By.cssSelector(".complete-header")).size() > 0);
    }
}