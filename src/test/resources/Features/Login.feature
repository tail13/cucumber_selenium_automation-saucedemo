Feature: User Login

  Scenario Outline: Successful Login
    Given the user on "https://www.saucedemo.com"
    When the user login with "<valid_username>" and "<valid_password>"
    Then the user should be logged in

    Examples: 
      | valid_username | valid_password |
      | standard_user  | secret_sauce   |
      | problem_user   | secret_sauce   |

  Scenario Outline: Invalid Login
    Given the user on "https://www.saucedemo.com"
    When the user login with "<invalid_username>" and "<invalid_password>"
    Then the user should see an error message

    Examples: 
      | invalid_username | invalid_password |
      | locked_out_user  | secret_sauce     |
      | not_user         | not_pass         |
