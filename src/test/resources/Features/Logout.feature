Feature: User Logout

  Background: 
    Given the user on "https://www.saucedemo.com"
    When the user login with "standard_user" and "secret_sauce"

  Scenario: Logout
    Given the user on the dashboard
    When the user clicks logout
    Then the user should be logged out