Feature: Shopping Cart

  Background: 
    Given the user on "https://www.saucedemo.com"
    When the user login with "standard_user" and "secret_sauce"

  Scenario: Adding products to cart
    Given the user on the dashboard
    When clicks the add to cart button
    And clicks the cart button
    Then the user could see product in cart list

  Scenario: Proceed to Checkout from the Shopping Cart
    Given the user on the dashboard
    When clicks the add to cart button
    And clicks the cart button
    Then the user could see product in cart list
    When the user click the checkout button
    Then the user should be taken to the checkout page
    When the user type their "fitst name", "last name", and "zip"
    And the user click continue
    Then the user should be taken to order detail page
    When the user click finish
    Then the user should see succesful order message
