Feature: Products Details

  Background: 
    Given the user on "https://www.saucedemo.com"
    When the user login with "standard_user" and "secret_sauce"

  Scenario: View Product Details
    Given the user on the dashboard
    When the user click the product title
    Then the user should be redirected to product detail page

  #Scenario: Sort Product list
    #Given the user on the dashboard
    #When the user click on "<value>" sort button
    #Then dashboard page should be sorted

    #Examples: 
    #  | value |
    #  | hilo  |
    #  | lohi  |
